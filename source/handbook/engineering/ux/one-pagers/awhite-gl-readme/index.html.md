---
layout: markdown_page
title: "Aaron K. White's README"
---

This page was inspired by the recent trend of Engineering Manager READMEs. _e.g._ [Hackernoon: 12 Manager READMEs (from some of the best cultures in tech)](https://hackernoon.com/12-manager-readmes-from-silicon-valleys-top-tech-companies-26588a660afe).

## Aaron K. White's README

Howdy! I'm Aaron and I'm the [UX Manager](/job-families/engineering/ux-management/), Ops (Configure, APM & Health) at GitLab.

This README is intended to be a **'How to Guide'** for getting to know me, answering some questions about me, and setting expectations about what it's like to work with me.

* [GitLab Handle](https://gitlab.com/awhite-gl)
* [My Website](https://aaronkwhite.com)
* [My Instagram](https://instagram.com/aaronkwhite)



### About Me

* I live in Littleton, CO with my wife Alison, our 7 yr old son Laine, and our lab pointer mix [Cider](/company/team-pets/#180-cider).
* I'm originally from San Antonio, TX, where my favorite time of year is Fiesta.
* I'm 6'8" and played basketball in college. Yes, the air is fresher up here...
* I try to spend as much time as I can Fly Fishing. There aren't many things in this world as peaceful as standing in a river trying to convince a fish to take your fly.
* I also enjoy Fly Tying. It is a great way to apply my creative skills to a physical/tactile activity.
* You can follow my [Instagram](https://instagram.com/aaronkwhite) to see my Flies.


### Working with me

* I have a degree in Computer Science and started my professional life as a web developer. I know enough JavaScript to be dangerous.
* I am a self-taught designer... Everything I know about Design and UX I learned on the mean streets.
* I believe that User Experience is a team sport and that the best ideas are found by including others.
* Product Discovery is my Jam! As a designer, I start every project with a discovery activity and quickly jump into story-mapping. As a manager, I will often ask about discovery artifacts and story-maps to make sure we're always starting from a place of deep empathy and shared understanding.
* I almost always have an opinion. However, I strive to provide feedback and explore alternatives rather than provide direction.
* I strongly believe that you were hired to be the UX expert in the room. My job is to help build you up and get to the next level (no matter what that is).
* My preferred method of communication is face-to-face (usually over a Zoom). I tend to be fairly responsive on Slack, but if it is urgent and you don’t hear from me, shoot me a text.


### Management style
* I fully embrace the GitLab value of [Transparency](/handbook/values/#transparency). My default mode is open and honest communication, however, there may be times where I can't share information.
* I prefer speed and honesty. If something isn't working, let's talk about it sooner rather than later. If there is something I need to change, please let me know as soon as possible.
* 1:1s are your time. These are your opportunities to let me know how you're doing, how you're feeling, what you need, what isn't working, what you think could be different, what your career goals are, and so on. I will usually have some things to talk about, but your needs come first.
