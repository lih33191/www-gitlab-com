---
layout: markdown_page
title: "GitLab raises $20 Million Series C Round Led by GV to Complete DevOps"
---

### WordPress Founder Matt Mullenweg Joins the GitLab Board as Company Momentum Hits an All-Time High

**SAN FRANCISCO - October 09, 2017** - Today GitLab, a software product used by [2/3 of all enterprises](/is-it-any-good/), has announced $20 million in series C funding led by GV at more than double the valuation of last year's B round. In addition, Matt Mullenweg, founder of WordPress, has joined the GitLab board to support the company’s mission to develop a seamless, integrated product for modern software developers and become the application for software development in Kubernetes.

As our world continues to become more software-defined and developers transform into the DevOps role, they need tools that accelerate workflow and simplify the development process. Since its incorporation in 2014, GitLab has shifted the software development process by transforming it into a seamless and collaborative conversation among developers. GitLab will use the funding to evolve the world of DevOps by offering both application development and operations. Where other products are focused on the intersection of Dev and Ops, GitLab will be the first single product to offer the union of Dev and Ops. GitLab will add new functionality for packaging, releasing, configuring, and monitoring software. More about this will be revealed in the live broadcast that will begin today at 10 a.m. PT and can be viewed [here](/blog/2017/10/09/complete-devops-live-event/).

“The Fortune 500 is racing to build world-class software development organizations that mirror the speed, productivity, and quality of the largest tech companies. As these organizations strive to produce high-quality code at scale, they will need best-in-class tools and platforms. GitLab’s platform accelerates the development process with an emphasis on collaboration and automation,” said Dave Munichiello, GV General Partner. “GitLab’s hybrid, multi-cloud solution is loved by developers, and is seeing tremendous traction in the field.”

Leading open source innovator, Matt Mullenweg joins GitLab’s board of directors to help drive the company’s long-term success as it continues to disrupt the developer ecosystem. Mullenweg, known as the father of open source, will bring his experience founding multiple companies including WordPress, to help guide GitLab’s open source strategy. 

“GitLab’s powerful momentum and scaling have a lot of parallels to Automattic and WordPress in their early days,” said Matt. “WordPress had to battle a lot of competitors, and ultimately came out on top as a successful company on an open source business model. I hope to help GitLab achieve the same triumph. Fundamentally, I want to help create the kind of internet that I want to live in and I want my children to live in, one that reaches a global audience and one that is able to make a difference.”

These announcements come as GitLab scales to meet the growing demand for a superior set of capabilities for modern software developers. Most recently with its deepening of the [executive bench](http://www.marketwired.com/press-release/gitlab-deepens-executive-bench-with-first-chief-culture-officer-chief-marketing-officer-2236346.htm) by adding a new chief marketing officer, new vice president of engineering and its first chief culture officer. GitLab was also named a leader in Continuous Integration by Forrester Research in [The Forrester Wave: Continuous Integration Tools, Q3 2017 report](/blog/2017/09/27/gitlab-leader-continuous-integration-forrester-wave/), GitLab provides the tools modern developers need to fully embrace the benefits of DevOps, including Kubernetes-based application development and monitoring. With the only single product for the modern software development life cycle (SDLC), GitLab helps organizations, whether a Fortune 500 enterprise or a one-person shop, embrace the power of DevOps, the cloud, and Kubernetes.

**About GitLab**  
Since its incorporation in 2014, GitLab has quickly become the leading self-hosted Git repository management tool used by software development teams ranging from startups to global enterprise organizations. GitLab has since expanded its product offering to deliver an integrated source code management, code review, test/release automation, and application monitoring product that accelerates and simplifies the software development process. With one end-to-end software development product, GitLab helps teams eliminate unnecessary steps from their workflow, significantly reduce cycle time and focus exclusively on building great software. Today, more than 100,000 organizations, including Ticketmaster, ING, NASDAQ, Alibaba, Sony, and Intel; and millions of users, trust GitLab to bring their modern applications from idea to production, reliably and repeatedly.

**Media Contact**  
Nicole Plati  
[press@gitlab.com](mailto:press@gitlab.com)  
415-963-4174 ext. 39
